#push a public key to ssh container
podman exec ssh_privatekey passwd -d root
podman cp sshpublic.key ssh_privatekey:/root/.ssh/authorized_keys
podman exec ssh_privatekey chown root:root /root/.ssh/authorized_keys

#add key to the keys folder
cp sshprivate.key ../../datatrucker_api/app/keys/sshprivate.key 

#create a tmp folder for sftp
mkdir /tmp/trucker
touch /tmp/trucker/sample.log